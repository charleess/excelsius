import React, { Component } from 'react';

class ComponentAdderButton extends Component {
    scrollToBottom() {
        window.scrollTo(0,document.body.scrollHeight);
    }

    render() {
        return (
            <div className="ComponentAdder" style={{position: 'fixed', bottom:'10px', right:'10px'}}>
                <a className="button is-primary" onClick={this.scrollToBottom.bind(this)}>Add a component</a>
            </div>
        );
    }
}

export default ComponentAdderButton;