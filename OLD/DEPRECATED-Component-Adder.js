import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {createVariable} from '../actions/actions-data';

class ComponentAdder extends Component {
    constructor(props){
        super(props);
        this.state = {
            modal: '',
            componentName: undefined,
            componentType: undefined,
            componentVars: undefined
        }
    }

    showModal() {
        this.setState({'modal': ` is-active slideInUp`})
    }

    closeModal() {
        this.setState({'modal': ` is-active slideOutDown`})
    }

    submitModal() {
        return true
    }

    handleChange(e, target) {
        var newPartialState = {};
        newPartialState[target] = e.target.value
        this.setState(newPartialState)
    }

    render() {
        console.log(this.state)
        return(
            <div className="ComponentAdder">
                <a onClick={this.showModal.bind(this)}>Add a new component</a>

                <div className={'modal animated' + this.state.modal}>
                    <div className="modal-background" style={{backgroundColor: 'transparent'}} onClick={this.closeModal.bind(this)}></div>
                    <div className="modal-card">
                        <header className="modal-card-head">
                            <p className="modal-card-title">Add a new component</p>
                            <button className="delete" onClick={this.closeModal.bind(this)}></button>
                        </header>
                        <section className="modal-card-body">
                            <div className="tile is-ancestor">
                                <div className="tile is-parent">
                                    <div className="tile is-child box field">
                                        <label className="label">Component Name</label>
                                        <p className="control">
                                            <input className={"input"} type="text" value={this.state.componentName} onChange={(e) => this.handleChange(e, 'componentName')}/>
                                        </p>
                                        <label className="label">Component Type</label>
                                        <p className="control">
                                            <span className="select">
                                                <select onChange={(e) => this.handleChange(e, 'componentType')}>
                                                    <option value='default'>Select an option</option>
                                                    <option value="pie-chart">Pie Chart</option>
                                                </select>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <footer className="modal-card-foot">
                            <a className="button is-success" onClick={this.submitModal.bind(this)}>Add Component</a>
                            <a className="button" onClick={this.closeModal.bind(this)}>Cancel</a>
                        </footer>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, props) {
    return {
        myData: state.components[props.name],
        state: state
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createVariable: createVariable
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ComponentAdder);
