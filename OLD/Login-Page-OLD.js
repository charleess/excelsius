import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import bcrypt from 'bcryptjs';

import { loginUser } from '../../actions/actions-login';

class LoginPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            credentials: {
                email: '',
                password: '',
                emailError: '',
                passwordError: ''
            }
        }
    }

    onInputChange(e){
        var field = e.target.name;
        var value = e.target.value
        var credentials = this.state.credentials;
        if(field === 'password'){
            bcrypt.genSalt(10, function(err, salt) {
                bcrypt.hash(value, salt, function(err, hash) {
                    credentials[field] = hash
                });
            });
        } else {
            credentials[field] = e.target.value
        }
        this.setState({credentials: credentials})
    }

    checkForm() {
        // //Add a bunch of tests to validate the form, it might be a good idea to pu it in another function than to keep it in a promise.
        // //Maybe separate functions for each field of the form
        // //Animqte the rejection
        // return new Promise((resolve, reject) => {
        //     if(isValid){
        //         resolve()
        //     } else {
        //         reject(new Error('this is an error'));
        //     }
        // })
    }

    onFormSubmit(e){
        e.preventDefault();
        this.checkForm().then(
            () => this.props.loginUser(this.state.credentials)
        ).catch(
            () => console.log('Error in the form')
        )
    }

    render() {
        return (
            <div className="LoginPage">
                <div className="columns">
                    <div className="column is-half is-offset-one-quarter">
                        <div className="content">
                            <p className="title">Login to Xcelsius</p>
                        </div>

                        <div className="field">
                            <label className="label">E-mail</label>
                            <p className="control">
                                <input 
                                    className="input"
                                    type="email"
                                    name="email"
                                    placeholder="E-mail"
                                    onChange={this.onInputChange.bind(this)}/>
                            </p>
                        </div>

                        <div className="field">
                            <label className="label">Password</label>
                            <p className="control">
                                <input
                                    className="input"
                                    type="password"
                                    name="password"
                                    placeholder="Password"
                                    onChange={this.onInputChange.bind(this)}/>
                            </p>
                        </div>

                        <div className="field">
                              <p className="control">
                                <button className="button is-primary" onClick={this.onFormSubmit.bind(this)}>Submit</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        state: state
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        loginUser: loginUser
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

import React, {Component} from 'react';
import PropTypes from 'prop-types'
import TextFieldGroup from '../common/text-field-group';
import validateInput from '../../../server/shared/validations/login';
import {connect} from 'react-redux';
import {login} from '../../actions/login-actions';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            identifier: "",
            password: "",
            errors: {},
            isLoading: false
        };

        this.onChange=this.onChange.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state);
        if (!isValid) {
            this.setState({errors});
        }
        return isValid
    }

    onSubmit (e) {
        e.preventDefault();
        // Client-side validation
        if (this.isValid()) {
            this.setState({errors: {}, isLoading: true});
            this.props.login(this.state).then(
                (res) => this.context.router.push('/'),
                (err) => this.setState({errors: err.response.data.errors, isLoading: false})
            );
        }
    }

    onChange (e) {
        this.setState({[e.target.name]: e.target.value})
    }

    render () {
        const {errors, identifier, password, isLoading} = this.state;

        return (
            <form onSubmit={this.onSubmit}>
                <h1>Login</h1>

                {errors.form && <div className="alert alert-danger">{errors.form}</div>}

                <TextFieldGroup 
                    field="identifier"
                    label="Username / Email"
                    value={identifier}
                    error={errors.identifier}
                    onChange={this.onChange}
                />

                <TextFieldGroup 
                    field="password"
                    label="Password"
                    type="password"
                    value={password}
                    error={errors.password}
                    onChange={this.onChange}
                />

                <div className="form-group">
                    <button className="btn btn-primary btn-lg" disabled={this.state.isLoading}>Login</button>
                </div>
            </form>
        )
    }
}

LoginForm.propTypes = {
    login: PropTypes.func.isRequired
}

LoginForm.contextTypes = {
    router: PropTypes.object.isRequired
}

export default connect(null, {login})(LoginForm);