import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MySlider from '../inputs/Slider';
import MyBarChart from '../charts/Bar-Chart';
import MyPieChart from '../charts/Pie-Chart';

import { updateComponentData, createVariable, createComponent } from '../../../actions/actions-data';


class GraphAndSliders extends Component {
  constructor(props){
    super(props);
    this.state = {
      modal: '',
      modalValid: true,
      id: props.id,
    }
  }

  componentWillMount() {
    var props = this.props

    // Setup stuff, will need to be deleted later
    // Check if the component exists, create it if not
    var createComponent = this.props.createComponent
    if(props.components[props.name] === undefined){
      createComponent(props.name, props.type, props.vars)
    }

    // Check if the variables exist, create them if not
    var createVariable = this.props.createVariable
    props.vars.map(function(v){
      if(props.variables[v] === undefined){
        createVariable(v)
      }
      return true
    })

    // // Map the variables to the state of the component
    // var initialState = {}
    // props.vars.map(function(v){
    //   if(props.variables[v] === undefined){
    //     initialState[v + 'min'] = 0;
    //     initialState[v + 'max'] = 100;
    //   } else {
    //     initialState[v + 'min'] = props.variables[v].min;
    //     initialState[v + 'max'] = props.variables[v].max;
    //   }
    //   initialState[v + 'minError'] = '';
    //   initialState[v + 'maxError'] = '';
    //   return true
    // })
    // this.setState(initialState)
  }

  showModal() {
    this.setState({'modal': ` is-active slideInUp`})
  }

  closeModal() {
    this.setState({'modal': ` is-active slideOutDown`})
  }

  changeName(e) {
    this.setState({'newName': e.target.value});
  }

  submitModal() {
    if(this.state.modalValid){
      this.props.updateComponentData(this.state.id, 'name', this.state.newName);
      this.setState({name: this.state.newName})
      this.closeModal();
    } else {
      console.log('Error in the form')
    }
  }

  render() {
    var graph
    switch(this.props.type){
        case 'pie-chart':
            graph = <MyPieChart vars={this.props.vars}/>
            break
        case 'bar-chart':
            graph = <MyBarChart vars={this.props.vars}/>
            break
        // no default
    }

    return (
      <div className="tile is-child box">
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
          <p className='title'>{this.state.name}</p>
          <a>
            <span className="icon is-pulled-right" onClick={this.showModal.bind(this)}>
              <i className="fa fa-cog"></i>
            </span>
          </a>
        </div>

        {this.props.vars.map((v) => (<MySlider key={v} id={v}/>))}
        {graph}

        <div className={'modal animated' + this.state.modal}>
          <div className="modal-background" onClick={this.closeModal.bind(this)}></div>
            <div className="modal-card">
              <header className="modal-card-head">
                <p className="modal-card-title">Parameters</p>
                <button className="delete" onClick={this.closeModal.bind(this)}></button>
              </header>
              <section className="modal-card-body">
                <div className="tile is-ancestor">
                  <div className="tile is-parent">
                    <div className="tile is-child box">
                      <div className="field">
                        <label className="label">New name</label>
                        <p className="control">
                            <input className={"input" + this.state.nameError} placeholder={this.state.name} type="text" onChange={this.changeName.bind(this)}/>
                        </p>
                        <p className={"help is-danger" + this.state.nameErrorHelp}>Please try another name</p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <footer className="modal-card-foot">
                <a className="button is-success" onClick={this.submitModal.bind(this)}>Save changes</a>
                <a className="button" onClick={this.closeModal.bind(this)}>Cancel</a>
              </footer>
            </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return {
        variables: state.variables,
        components: state.components
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateComponentData: updateComponentData,
        createVariable: createVariable,
        createComponent: createComponent
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(GraphAndSliders);
