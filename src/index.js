import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { persistStore, autoRehydrate } from 'redux-persist'
import { BrowserRouter } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { allReducers } from './reducers';

import App from './router/App';

let history = createBrowserHistory();

let store = createStore(
    allReducers, 
    composeWithDevTools(
        applyMiddleware(thunk),
        autoRehydrate()
    )
);

persistStore(store)

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter history={history}>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));
    
registerServiceWorker();
