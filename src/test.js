import React, { Component } from 'react';

class test extends Component {
    render(){
        return (
            <div>
                Hello
            </div>
        )
    }
}

export default test




    validateDataName() {
        var formValid = this.state.formValid

        // The name must be a new one, not empty, not null
        var currentComponentsNames = Object.keys(this.props.components).map(function(comp) {return this.props.components[comp].name}.bind(this))
        if(currentComponentsNames.indexOf(this.state.newName) > -1 || this.state.newName === '' || this.state.newName === undefined){
            this.setState({nameError: ` is-danger`, nameErrorHelp: '', formValid: false});
            formValid = false;
        } else {
            this.setState({nameErrorHelp: ` is-hidden`, formValid: true});
            formValid = true;
        }

        if(formValid){
            this.props.updateComponentData(this.state.id, 'name', this.state.newName);
        }
    }

    //Variables
    handleNumberOfVariablesChange(e) {
        var oldNumberOfVars = this.state.numberOfVariables;
        var arrayVars = this.state.variables;
        if(oldNumberOfVars < Number(e.target.value)){
            for(var i = 1; i <= Number(e.target.value) - oldNumberOfVars; i++){
                arrayVars.push('');
            }
            this.setState({numberOfVariables: Number(e.target.value), variables: arrayVars})
        } else if (oldNumberOfVars > Number(e.target.value)){
            for(i = 1; i <= oldNumberOfVars - Number(e.target.value); i++){
                arrayVars.pop();
            }
            this.setState({numberOfVariables: Number(e.target.value), variables: arrayVars})
        }
        this.props.updateComponentData(this.state.id, 'numberOfVars', Number(e.target.value));
        this.props.updateComponentData(this.state.id, 'vars', arrayVars);
    }

    bindVariableToComponent(e) {
        var varArray = this.state.variables;
        varArray[e.target.getAttribute('id')] = e.target.value
        this.setState({'variables': varArray})
        this.props.updateComponentData(this.state.id, 'vars', varArray)
    }

    //Type
    handleTypeChange(e) {
        this.setState({type: e.target.value})
    }

    validateTypeChange() {
        this.props.updateComponentData(this.state.id, 'type', this.state.type)
        this.setState({icon: icons[this.state.type]})
    }



    render() {
        var myComponent = this.props.components[this.props.id];
        const {name, errors, isLoading} = this.state
        const isLoadingClass = isLoading ? ` is-loading` : ""

        return (
            <div className="tile is-child box">
                <span className="icon is-pulled-right">
                        <i className={"fa" + this.state.icon}></i>
                </span>
                <p className="title">{myComponent.name}</p>
                <div className="tabs is-centered">
                    <ul>
                        <li className={this.state.view === 'name' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='name'>Name</a></li>
                        <li className={this.state.view === 'variables' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='variables'>Variables</a></li>
                        <li className={this.state.view === 'type' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='type'>Type</a></li>
                    </ul>
                </div>

                <div className={this.state.view === 'name' ? "field" : "field is-hidden"}>
                    <TextInput
                        label='New Name'
                        type='text'
                        name='name'
                        value={name}
                        placeholder='Name'
                        onChange={this.onChange.bind(this)}
                    />

                    <a className="button is-primary is-pulled-right" onClick={this.onSubmit.bind(this)}>Update</a>

                </div>
                    
                <div className={this.state.view === 'variables' ? "field" : "field is-hidden"}>
                    <label className="label">Number of variables</label>
                    <p className="control">
                        <span className={"select" + this.state.numberOfVariablesError}>
                            <select value={this.state.numberOfVariables} onChange={this.handleNumberOfVariablesChange.bind(this)}>
                                <option value={0}>Select the number</option>
                                <option value={1}>1</option>
                                <option value={2}>2</option>
                                <option value={3}>3</option>
                                <option value={4}>4</option>
                                <option value={5}>5</option>
                            </select>
                        </span>
                    </p>
                </div>
                <div className={this.state.view === 'variables' ? "field" : "field is-hidden"}>
                    <label className="label">{this.state.numberOfVariables === 0 ? "" : "Mapping"}</label>
                    {this.state.variables.map(function(variable, i){
                        return (
                            <p key={i+1} className="control" style={{display: "flex", justifyContent: "space-around"}}>
                                <a className="button is-white" disabled>Variable {i+1}</a>
                                <span className={"select"}>
                                    <select id={i} value={this.props.components[this.state.id].vars[i]} onChange={this.bindVariableToComponent.bind(this)}>
                                        <option value="default">Select a variable</option>
                                        {Object.keys(this.props.variables).sort().map(function(opt){
                                            return (
                                                <option key={opt + "child"} value={opt}>{this.props.variables[opt].name}</option>
                                            )
                                        }.bind(this))}
                                    </select>
                                </span>
                            </p>
                        )
                    }.bind(this))}
                </div>
                <div className={this.state.view === 'variables' ? "field" : "field is-hidden"}>
                    <p className="field">
                        <a className="button is-primary is-pulled-right" disabled>Update is automatic</a>
                    </p>
                </div>

                <div className={this.state.view === 'type' ? "field" : "field is-hidden"}>
                    <label className="label">Change the type of component</label>
                    <p className="field">The current type is {this.props.componentsTypes[this.props.components[this.state.id].type].name}</p>
                    <p className="control">
                        <span className={"select"}>
                            <select value={this.state.type} onChange={this.handleTypeChange.bind(this)}>
                                {Object.keys(this.props.componentsTypes).map(function(type){
                                    return (
                                        <option key={type} value={type}>{this.props.componentsTypes[type].name}</option>
                                        )
                                }.bind(this))}
                            </select>
                        </span>
                    </p>
                </div>
                <div className={this.state.view === 'type' ? "field" : "field is-hidden"}>
                    <p className="field">
                        <a className="button is-primary is-pulled-right" onClick={this.validateTypeChange.bind(this)}>Update</a>
                    </p>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
         components: state.components,
         componentsTypes: state.componentsTypes,
         variables: state.variables
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateComponentData: updateComponentData,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ComponentEditor);
