import React, { Component } from 'react';
import { connect } from 'react-redux';

import ComponentEditor from './Component-Editor';
import ComponentCreator from './Component-Creator';

class ComponentsGrid extends Component{

    sliceComponents(compArray){
        var j = compArray.length;
        var tempArray;
        var gridArray = [];
        for(var i = 0; i < j; i += 3){
            tempArray = compArray.slice(i, i + 3);
            gridArray.push(tempArray)
        }
        return(gridArray.map(function(arr) {return (
            <div key={arr} className='tile is-ancestor'>
                {this.displayLine(arr)}
            </div>
            )
        }.bind(this)))
    }

    displayLine(tempArray) {
        return tempArray.map(function(key) {
            return(
                    <div key={key} className="tile is-4 is-parent">
                        <ComponentEditor id={key}/>
                    </div>
            )
        })
    }

	render() {
        return (
            <div className="container" style={{marginTop: '5px'}}>
                {this.sliceComponents(Object.keys(this.props.components).sort())}
                <div className='tile is-ancestor'>
                    <div className="tile is-4 is-parent">
                        <div className="tile is-child box">
                            <ComponentCreator/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        components: state.components
    }
}

export default connect(mapStateToProps)(ComponentsGrid);