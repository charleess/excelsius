import React, { Component } from 'react';
import validateInput from '../../misc/validators/components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import TextInput from '../../misc/text-input';
import DropDownInput from '../../misc/drop-down-input';

import { updateComponentData, deleteComponent } from '../../actions/actions-data';
import icons from '../../misc/icons';

class ComponentEditor extends Component {
    constructor(props){
        super(props);
        var myComponent = props.components[props.id];
        this.state = {
            id: this.props.id,
            icon: icons[myComponent.type],
            formValid: true,
            type: myComponent.type,
            name: myComponent.name,
            variables: myComponent.vars,
            numberOfVariables: myComponent.numberOfVars,
            view: 'name',
            errors: {}
        }
    }

    //General
    switchView(e) {
        this.setState({view: e.target.getAttribute('value')})
    }

    onChange(e) {
        if(e.target.name === 'numberOfVariables') {
            var oldNumberOfVars = this.state.numberOfVariables;
            var arrayVars = this.state.variables;
            if(oldNumberOfVars < Number(e.target.value)){
                for(var i = 1; i <= Number(e.target.value) - oldNumberOfVars; i++){
                    arrayVars.push('');
                }
                this.setState({variables: arrayVars})
            } else if (oldNumberOfVars > Number(e.target.value)){
                for(i = 1; i <= oldNumberOfVars - Number(e.target.value); i++){
                    arrayVars.pop();
                }
                this.setState({variables: arrayVars})
            } 
            this.props.updateComponentData(this.state.id, 'numberOfVars', Number(e.target.value));
        } 
        this.setState({[e.target.name]: e.target.value})
    }

    onDelete() {
        this.props.deleteComponent(this.props.id)
    }

    onSubmit() {
        // Client-side validation
        if (this.isValid()) {
            this.setState({errors: {}, isLoading: true});
            this.props.updateComponentData(this.state.id, 'name', this.state.name);
            this.props.updateComponentData(this.state.id, 'numberOfVars', this.state.numberOfVariables);
            this.props.updateComponentData(this.state.id, 'vars', this.state.variables);
            this.props.updateComponentData(this.state.id, 'type', this.state.type)
            this.setState({isLoading: false})
        }
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state, this.props.components, this.state.id);
        if (!isValid) {
            this.setState({errors});
        }
        return isValid
    }

    bindVariableToComponent(e) {
        var varArray = this.state.variables;
        varArray[e.target.getAttribute('id')] = e.target.value
        this.setState({'variables': varArray})
        this.props.updateComponentData(this.state.id, 'vars', varArray)
    }

    render() {
        var myComponent = this.props.components[this.props.id];
        const {name, type, numberOfVariables, errors, view} = this.state

        return (
            <div className="tile is-child box">
                <a>
                    <span className="icon is-pulled-right" onClick={this.onDelete.bind(this)}>
                            <i className={"fa fa-trash"}></i>
                    </span>
                </a>
                <span className="icon is-pulled-right">
                        <i className={"fa" + this.state.icon}></i>
                </span>

                <p className="title">{myComponent.name}</p>
                <div className="tabs is-centered">
                    <ul>
                        <li className={view === 'name' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='name'>Name</a></li>
                        <li className={view === 'variables' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='variables'>Variables</a></li>
                        <li className={view === 'type' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='type'>Type</a></li>
                    </ul>
                </div>

                <div className={view === 'name' ? "field" : "field is-hidden"}>
                    <TextInput
                        label='New Name'
                        type='text'
                        name='name'
                        value={name}
                        placeholder='Name'
                        onChange={this.onChange.bind(this)}
                        error={errors.name}
                    />

                    <a className="button is-primary is-pulled-right" onClick={this.onSubmit.bind(this)}>Update</a>
                </div>
                    
                <div className={view === 'variables' ? "field" : "field is-hidden"}>
                     <DropDownInput
                        label='Number of Variables'
                        name="numberOfVariables"
                        value={numberOfVariables}
                        options={[0,1,2,3,4,5]}
                        optionsLabels={{0:{name: "0"}, 1:{name: "1"}, 2:{name: "2"}, 3:{name: "3"}, 4:{name: "4"}, 5:{name: "5"}}}
                        onChange={this.onChange.bind(this)}
                        error={errors.numberOfVariables} /> 
                        
                </div>
                <div className={view === 'variables' ? "field" : "field is-hidden"}>
                    <label className="label">{this.state.numberOfVariables === 0 ? "" : "Mapping"}</label>
                    {this.state.variables.map(function(variable, i){
                        return (
                            <p key={i+1} className="control" style={{display: "flex", justifyContent: "space-around"}}>
                                <a className="button is-white" disabled>Variable {i+1}</a>
                                <span className={"select"}>
                                    <select id={i} value={this.props.components[this.state.id].vars[i]} onChange={this.bindVariableToComponent.bind(this)}>
                                        <option value="">Select a variable</option>
                                        {Object.keys(this.props.variables).sort().map(function(opt){
                                            return (
                                                <option key={opt + "child"} value={opt}>{this.props.variables[opt].name}</option>
                                            )
                                        }.bind(this))}
                                    </select>
                                </span>
                            </p>
                        )
                    }.bind(this))}
                    <a className="button is-primary is-pulled-right" style={{marginTop: '14px'}} disabled>Update is automatic</a>
                </div>

                <div className={view === 'type' ? "field" : "field is-hidden"}>
                    <p className="field">The current type is {this.props.componentsTypes[this.props.components[this.state.id].type].name}</p>
                     <DropDownInput
                        label='Change the type of component'
                        name="type"
                        value={type}
                        options={Object.keys(this.props.componentsTypes).sort().map((ct) => ct)}
                        optionsLabels={this.props.componentsTypes}
                        onChange={this.onChange.bind(this)}
                        error={errors.type} /> 

                    <a className="button is-primary is-pulled-right" onClick={this.onSubmit.bind(this)}>Update</a>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
         components: state.components,
         componentsTypes: state.componentsTypes,
         variables: state.variables
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateComponentData: updateComponentData,
        deleteComponent: deleteComponent
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ComponentEditor);
