import React, { Component } from 'react';
import validateInput from '../../misc/validators/components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import TextInput from '../../misc/text-input';
import DropDownInput from '../../misc/drop-down-input';
import ModalParameters from '../../misc/modal-parameters';

import { createComponent } from '../../actions/actions-data';

class GenericComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            modal: '',
            name: '',
            type: '',
            isLoading: false,
            errors: {}
        }
    }

    showModal() {
        this.setState({'modal': ` is-active slideInUp`})
    }

    closeModal() {
        this.setState({'modal': ` is-active slideOutDown`})
    }

    submitModal() {
        return true
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit() {
        // Client-side validation
        if (this.isValid()) {
            this.setState({errors: {}, name: '', isLoading: true});
            this.props.createComponent(this.state.name, this.state.type, undefined)
            this.setState({isLoading: false})
        }
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state, this.props.components);
        if (!isValid) {
            this.setState({errors});
        }
        return isValid
    }

    render() {
        const {name, errors, isLoading} = this.state
        const isLoadingClass = isLoading ? ` is-loading` : ""

        return (
            <div className="Genric-component">
                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                    <p className='title'>Add Component</p>
                    <a>
                        <span className="icon is-pulled-right" onClick={this.showModal.bind(this)}>
                            <i className="fa fa-cog"></i>
                        </span>
                    </a>
                </div>
                <div className="new">

                    <TextInput
                        label='Name'
                        type='text'
                        name='name'
                        value={name}
                        placeholder='Name'
                        onChange={this.onChange.bind(this)}
                        error={errors.name} />

                    <DropDownInput
                        label='Component Type'
                        name='type'
                        defaultOption='Select a component Type'
                        options={Object.keys(this.props.componentsTypes).sort().map((ct) => ct)}
                        optionsLabels={this.props.componentsTypes}
                        onChange={this.onChange.bind(this)}
                        error={errors.type} />

                    <a className={"button is-primary is-pulled-right" + isLoadingClass} onClick={this.onSubmit.bind(this)}>Create</a>
                </div>

                <ModalParameters
                    animation={this.state.modal}
                    hide={this.closeModal.bind(this)}
                    submit={this.submitModal.bind(this)} />

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        components: state.components,
        componentsTypes: state.componentsTypes
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createComponent: createComponent
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(GenericComponent);
