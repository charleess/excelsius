import React, {Component} from 'react';
import validateInput from '../../misc/validators/login';
import { bindActionCreators } from 'redux'
import { connect} from 'react-redux';
import TextInput from '../../misc/text-input';
// import { loginUser } from '../../actions/actions-login';

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            errors: {},
            isLoading: false
        };
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state);
        if (!isValid) {
            this.setState({errors});
        }
        return isValid
    }

    onSubmit (e) {
        e.preventDefault();
        // Client-side validation
        if (this.isValid()) {
            this.setState({errors: {}, isLoading: true});
            // this.props.login(this.state).then(
            //     (res) => this.context.router.push('/'),
            //     (err) => this.setState({errors: err.response.data.errors, isLoading: false})
            // );
        }
    }

    onChange (e) {
        this.setState({[e.target.name]: e.target.value})
    }

    render () {
        const {errors, email, password, isLoading} = this.state;
        const isLoadingClass = isLoading ? ` is-loading` : ""
        
        return (
            <div className="LoginPage">
                <div className="columns">
                    <div className="column is-half is-offset-one-quarter">
                        <div className="content">
                            <p className="title">Login to Xcelsius</p>
                            {errors.form && <div className="alert alert-danger">{errors.form}</div>}
                        </div>

                        <TextInput
                            label='E-mail'
                            type='text'
                            name='email'
                            value={email}
                            placeholder='E-mail'
                            onChange={this.onChange.bind(this)}
                            error={errors.email} />

                        <TextInput
                            label='Password'
                            type='password'
                            name='password'
                            value={password}
                            placeholder='Password'
                            onChange={this.onChange.bind(this)}
                            error={errors.password} />

                        <a className={"button is-primary" + isLoadingClass} onClick={this.onSubmit.bind(this)}>Submit</a>
                        
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {

    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);