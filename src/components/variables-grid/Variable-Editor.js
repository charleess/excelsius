import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { updateVariableData, deleteVariable } from '../../actions/actions-data';

class VariableEditor extends Component {
    constructor(props){
        super(props);
        var myVariable = props.variables[props.id];
        this.state = {
            id: this.props.id,
            view: 'name',
            formValid: true,
            calculated: myVariable.calculated,
            newName: myVariable.name,
            nameError: '',
            nameErrorHelp: ` is-hidden`,
            newMin: myVariable.min,
            minError: '',
            minErrorHelp: ` is-hidden`,
            newMax: myVariable.max,
            maxError: '',
            maxErrorHelp: ` is-hidden`,
            crossErrorHelp: ` is-hidden`,
            newUnit: '',
            unitError: '',
            unitErrorHelp: ` is-hidden`
        }
    }
    
    //General
    switchView(e) {
        this.setState({view: e.target.getAttribute('value')})
    }

    changeName(e) {
        this.setState({'newName': e.target.value})
    }

    changeMin(e) {
        this.setState({'newMin': e.target.value})
    }

    changeMax(e) {
        this.setState({'newMax': e.target.value})
    }

    changeUnit(e) {
        this.setState({'newUnit': e.target.value})
    }


    validateDataName() {
        var formValid = this.state.formValid
        //TODO: ADD A CHECK, IF THE NAMES DID NOT CHANGE, LEAVE IT. FOR NOW IT RAISES AN ERROR
        // The name must be a new one, not empty, not null
        var currentVariablesNames = Object.keys(this.props.variables).map(function(variable) {return this.props.variables[variable].name}.bind(this))
        if(currentVariablesNames.indexOf(this.state.newName) > -1 || this.state.newName === '' || this.state.newName === undefined){
            this.setState({nameError: ` is-danger`, nameErrorHelp: '', formValid: false});
            formValid = false;
        } else {
            this.setState({nameErrorHelp: ` is-hidden`, nameError: '', formValid: true});
            formValid = true;
        }

        if(formValid){
            this.props.updateVariableData(this.state.id, 'name', this.state.newName);
        }
    }

    validateDataExtrema() {
        var formValid = this.state.formValid
        // The min/max must be coherent, not null, not empty
        // Check the minimum
        if(isNaN(this.state.newMin) || this.state.newMin === ''){
            this.setState({minError: ` is-danger`, minErrorHelp: '', formValid: false});
            formValid = false
        } else {
            this.setState({minError: '', minErrorHelp: ` is-hidden`, formValid: true});
            formValid = true;
        }
        // Check the maximum
        if(isNaN(this.state.newMax) || this.state.newMax === ''){
            this.setState({maxError: ` is-danger`, maxErrorHelp: '', formValid: false});
            formValid = false;
        } else {
            this.setState({maxError: '', maxErrorHelp: ` is-hidden`, formValid: true});
            formValid = true;
        }
        // Check the coherence
        if(this.state.newMin > this.state.newMax){
            this.setState({minError: ` is-danger`, maxError: ` is-danger`, crossErrorHelp: '', formValid: false});
            formValid = false;
        } else {
            this.setState({crossErrorHelp: ` is-hidden`, minError: '', maxError: '', formValid: true});
            formValid = true;
        }

        if(formValid){
            this.props.updateVariableData(this.state.id, 'min', Number(this.state.newMin));
            this.props.updateVariableData(this.state.id, 'max', Number(this.state.newMax));
        }
    }

    onDelete() {
        this.props.deleteVariable(this.props.id)
    }

    validateDataUnit() {
        //Add a validation, change with a dropdown list rather than a blank field
    }

    render() {
        var myVariable = this.props.variables[this.props.id];
        var {view} = this.state
        return (
            <div className="tile is-child box">
                <a>
                    <span className="icon is-pulled-right" onClick={this.onDelete.bind(this)}>
                            <i className={"fa fa-trash"}></i>
                    </span>
                </a>
                <p className="title">{myVariable.name}</p>
                <div className="tabs is-centered">
                    <ul>
                        <li className={view === 'name' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='name'>Name</a></li>
                        <li className={view === 'extrema' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='extrema'>Extrema</a></li>
                        <li className={view === 'unit' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='unit'>Unit</a></li>
                        {this.state.calculated ? <li className={view === 'formula' ? "is-active" : ""} onClick={this.switchView.bind(this)}><a value='formula'>Formula</a></li> : ""}
                    </ul>
                </div>

                <div className={view === 'name' ? "field" : "field is-hidden"}>
                    <label className="label">New name</label>
                    <p className="control">
                        <input className={"input" + this.state.nameError} value={this.state.newName} type="text" onChange={this.changeName.bind(this)}/>
                    </p>
                    <p className={"help is-danger" + this.state.nameErrorHelp}>Please try another name</p>
                </div>
                <div className={view === 'name' ? "field" : "field is-hidden"}>
                    <p className="field">
                        <a className="button is-primary is-pulled-right" onClick={this.validateDataName.bind(this)}>Update</a>
                    </p>
                </div>

                <div className={view === 'extrema' ? "field" : "field is-hidden"}>
                    <label className="label">Minimum</label>
                    <p className="control">
                        <input className={"input" + this.state.minError} type="text" value={this.state.newMin} onChange={this.changeMin.bind(this)}/>
                    </p>
                    <p className={"help is-danger" + this.state.minErrorHelp}>The minimum is not valid</p>

                    <label className="label">Maximum</label>
                    <p className="control">
                        <input className={"input" + this.state.maxError} type="text" value={this.state.newMax} onChange={this.changeMax.bind(this)}/>
                    </p>
                    <p className={"help is-danger" + this.state.maxErrorHelp}>The maximum is not valid</p>
                    <p className={"help is-danger" + this.state.crossErrorHelp}>The minimum and maximum are not coherent</p>
                </div>
                <div className={view === 'extrema' ? "field" : "field is-hidden"}>
                    <p className="field">
                        <a className="button is-primary is-pulled-right" onClick={this.validateDataExtrema.bind(this)}>Update</a>
                    </p>
                </div>

                <div className={view === 'unit' ? "field" : "field is-hidden"}>
                    <label className="label">Unit</label>
                    <p className="control">
                        <input className={"input" + this.state.unitError} value={this.state.newUnit} type="text" onChange={this.changeUnit.bind(this)}/>
                    </p>
                    <p className={"help is-danger" + this.state.unitErrorHelp}>Please try another unit</p>
                </div>
                <div className={view === 'unit' ? "field" : "field is-hidden"}>
                    <p className="field">
                        <a className="button is-primary is-pulled-right" onClick={this.validateDataUnit.bind(this)}>Update</a>
                    </p>
                </div>

                <div className={view === 'formula' ? "field" : "field is-hidden"}>
                    <label className="label">Formula</label>
                    <p className="control">
                        <input className={"input"} type="text" />
                    </p>
                    <p className={"help is-danger"}>Please try another unit</p>
                </div>
                <div className={view === 'formula' ? "field" : "field is-hidden"}>
                    <p className="field">
                        <a className="button is-primary is-pulled-right">Update</a>
                    </p>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
         variables: state.variables
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateVariableData: updateVariableData,
        deleteVariable: deleteVariable
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(VariableEditor);
