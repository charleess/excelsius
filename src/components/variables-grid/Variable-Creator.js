import React, { Component } from 'react';
import validateInput from '../../misc/validators/variables';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import TextInput from '../../misc/text-input';
import ModalParameters from '../../misc/modal-parameters';

import { createVariable } from '../../actions/actions-data';

class GenericVariable extends Component {
    constructor(props){
        super(props);
        this.state = {
            modal: '',
            name: '',
            calculated: false,
            errors: {},
            isLoading: false
        }
    }

    showModal() {
        this.setState({'modal': ` is-active slideInUp`})
    }

    closeModal() {
        this.setState({'modal': ` is-active slideOutDown`})
    }

    submitModal() {
        return true
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    onChangeCalc() {
        this.setState({calculated: !this.state.calculated})
    }

    onSubmit(e) {
        e.preventDefault();
        // Client-side validation
        if (this.isValid()) {
            this.setState({errors: {}, name: '', isLoading: true});
            this.props.createVariable(this.state.name, this.state.calculated)
            this.setState({isLoading: false})
        }
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state, this.props.variables);
        if (!isValid) {
            this.setState({errors});
        }
        return isValid
    }

    render() {
        console.log(this.state)
        const {name, errors, isLoading} = this.state
        const isLoadingClass = isLoading ? ` is-loading` : ""

        return (
            <div className="Generic-variable">
                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                    <p className='title'>Add Variable</p>
                    <a>
                        <span className="icon is-pulled-right" onClick={this.showModal.bind(this)}>
                            <i className="fa fa-cog"></i>
                        </span>
                    </a>
                </div>

                <div className="new">
                    <label className="label">Create a new variable</label>

                    <TextInput
                        label='Name'
                        type='text'
                        name='name'
                        value={name}
                        placeholder='Name'
                        onChange={this.onChange.bind(this)}
                        error={errors.name} />

                    <label className="checkbox">
                        <input type="checkbox" name='calculated' onChange={this.onChangeCalc.bind(this)}/>
                        &nbsp;Calculated Variable
                    </label>

                    <a className={"button is-primary is-pulled-right" + isLoadingClass} onClick={this.onSubmit.bind(this)}>Create</a>
                </div>


                <ModalParameters
                    animation={this.state.modal}
                    hide={this.closeModal.bind(this)}
                    submit={this.submitModal.bind(this)} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        variables: state.variables,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createVariable: createVariable
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(GenericVariable);
