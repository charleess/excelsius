import React, { Component } from 'react';
import { connect } from 'react-redux';

import VariableEditor from './Variable-Editor';
import VariableCreator from './Variable-Creator';

class VariablesGrid extends Component{

    sliceVariables(varArray){
        var j = varArray.length;
        var tempArray;
        var gridArray = [];
        for(var i = 0; i < j; i += 3){
            tempArray = varArray.slice(i, i + 3);
            gridArray.push(tempArray)
        }
        return(
            gridArray.map(function(arr) {
                return (
            <div key={arr} className='tile is-ancestor'>
                {this.displayLine(arr)}
            </div>
            )
        }.bind(this)))
    }

    displayLine(tempArray) {
        return tempArray.map(function(key) {
            return(
                    <div key={key} className="tile is-4 is-parent">
                        <VariableEditor id={key}/>
                    </div>
            )
        })
    }

	render() {
        return (
            <div className="container" style={{marginTop: '5px'}}>
                {this.sliceVariables(Object.keys(this.props.variables).sort())}
                <div className='tile is-ancestor'>
                    <div className="tile is-4 is-parent">
                        <div className="tile is-child box">
                            <VariableCreator/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        variables: state.variables
    }
}

export default connect(mapStateToProps)(VariablesGrid);