import React, { Component } from 'react';
import { connect} from 'react-redux';

import ComponentWrapper from './comps/Component-Wrapper';
import ComponentAdderTile from './comps/Component-Adder-Tile';

class AppGrid extends Component {

    sliceComponents(compArray){
        var j = compArray.length;
        var tempArray;
        var gridArray = [];
        for(var i = 0; i < j; i += 3){
            tempArray = compArray.slice(i, i + 3);
            gridArray.push(tempArray)
        }

        return(gridArray.map(function(arr) {return (
            <div key={arr} className='tile is-ancestor'>
                {this.displayLine(arr)}
            </div>
            )
        }.bind(this)))
    }

    displayLine(tempArray) {
        return tempArray.map(function(key) {
            return(
                    <div key={key} className="tile is-parent">
                        <ComponentWrapper id={key}/>
                    </div>
            )
        })
    }

    render() {
        return (
            <div className="container" style={{marginTop: '5px'}}>
                {this.sliceComponents(Object.keys(this.props.grid).sort())}
                <div className="tile is-ancestor">
                    <div className="tile is-parent is-4">
                        <div className="tile is-child box">
                            <ComponentAdderTile />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        grid: state.appGrid
    }
}

export default connect(mapStateToProps)(AppGrid);