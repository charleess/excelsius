import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { BarChart, Bar, XAxis, YAxis, Tooltip, ResponsiveContainer } from 'recharts';

class MyBarChart extends Component{
	render() {
        // These are all the variables in the app
        var myData = this.props.variables;
        // These are the variables used in the graph
        var myVars = this.props.vars;
        // Mapping the correct variables to the graph
        var data = myVars.map(function(v){
            return {'name': myData[v].name, 'value': myData[v].value}
        })
        //Remove blanks
        data = data.filter(function(n){ return n !== undefined });

        return (
            <ResponsiveContainer width="100%" height={300}>
                <BarChart width={800} height={400} data={data}>
                    <XAxis tickLine={false} font-family="Helvetica" dataKey="name"/>
                    <YAxis tickLine={false} />
                    <Tooltip/>
                    <Bar dataKey="value" fill="#23D160" />
                </BarChart>
            </ResponsiveContainer>
        );
  }
}

function mapStateToProps(state) {
    return {
        variables: state.variables
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MyBarChart);