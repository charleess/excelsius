import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { PieChart, Pie, ResponsiveContainer } from 'recharts';

class MyPieChart extends Component{
	render() {
        // These are all the variables in the app
        var myData = this.props.variables;
        // These are the variables used in the graph
        var myVars = this.props.vars;
        // Mapping the correct variables to the graph
        var data = myVars.map(function(v){
            return {'name': myData[v].name, 'value': myData[v].value}
        })

        return (
            <ResponsiveContainer width="100%" height={300}>
                <PieChart width={800} height={400}>
                    <Pie data={data} nameKey='name' dataKey='value' cx='50%' cy='50%' innerRadius={70} outerRadius={90} fill="#23D160"/>
                </PieChart>
            </ResponsiveContainer>
        );
  }
}

function mapStateToProps(state) {
    return {
        variables: state.variables
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MyPieChart);