import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactEcharts from 'echarts-for-react';

class BarChart2 extends Component {
    getOption() {
        var myData = this.props.variables;

        var data = JSON.stringify(this.props.vars.map(function(v){
                return {
                    type: 'bar',
                    name: myData[v].name,
                    data: myData[v].value
                }
            }))

        var result = {
            tooltip: {},
            legend: {
                data: ["Data"]
            },
            xAxis: {
                type: 'category',
                data: this.props.vars.map(function(v){return myData[v].name})
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                name: 'Data',
                type: "bar",
                data: this.props.vars.map(function(v){return myData[v].value})
            }]
        }

        return result
    }

    render() {
        var option = this.getOption.bind(this)
        return (
            <ReactEcharts
                option={option()}
                notMerge={true}
                lazyUpdate={true} />
        )
    }
}

function mapStateToProps(state) {
    return {
        variables: state.variables
    }
}

export default connect(mapStateToProps)(BarChart2)