import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactEcharts from 'echarts-for-react';

class PieChart2 extends Component {
    getOption() {
        var myData = this.props.variables;

        var result = {
            tooltip: {},
            legend:{
                data: this.props.vars.map(function(v){return myData[v].name})
            },
            series: [{
                type: 'pie',
                radius: '55%',
                // roseType: 'angle',
                data: this.props.vars.map(function(v){return {value: myData[v].value, name: myData[v].name} }),
                itemStyle: {
                //     normal: {
                //         // shadow size
                //         shadowBlur: 200,
                //         // horizontal offset of shadow
                //         shadowOffsetX: 0,
                //         // vertical offset of shadow
                //         shadowOffsetY: 0,
                //         // shadow color
                //         shadowColor: 'rgba(0, 0, 0, 0.5)'
                //     }
                        emphasis: {
                            shadowBlur: 200,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                }
            }]
        }

        return result
    }

    render() {
        var option = this.getOption.bind(this)
        return (
            <ReactEcharts
                option={option()}
                notMerge={true}
                lazyUpdate={true} />
        )
    }
}

function mapStateToProps(state) {
    return {
        variables: state.variables
    }
}

export default connect(mapStateToProps)(PieChart2)