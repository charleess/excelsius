import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Slider from 'rc-slider';

import {updateVariableData} from '../../../actions/actions-data';

import 'rc-slider/assets/index.css';

class MySlider extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: props.variables[props.id].value,
        }
    }

    componentWillReceiveProps(){
        this.setState({value: this.props.variables[this.props.id].value})
    }

    handleChange(e){
        this.setState({value: e})
    }

    handleAfterChange(e){
        this.props.updateVariableData(this.props.id, 'value', e)
    }

    render() {
        return (
            <div className='Slider-wrapper' style={{display: 'flex', alignItems: 'center'}}>
                <Slider
                    onChange={this.handleChange.bind(this)}
                    onAfterChange={this.handleAfterChange.bind(this)}
                    value={this.state.value}
                    min={this.props.variables[this.props.id].min}
                    max={this.props.variables[this.props.id].max}/>
                <p className='content' style={{minWidth: '50px', textAlign: 'center'}}>{this.state.value}</p>
            </div>
        );
    }

};

function mapStateToProps(state) {
    return {
        variables: state.variables,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateVariableData: updateVariableData
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MySlider);
