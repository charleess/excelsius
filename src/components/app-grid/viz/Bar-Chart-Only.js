import React, { Component } from 'react';
import { connect } from 'react-redux';
// import MyBarChart from '../charts/Bar-Chart';

import BarChart2 from '../charts/Bar-Chart-V2';

class BarChartOnly extends Component {

  render() {
      var thisComponent = this.props.components[this.props.id]
        return (
            // <MyBarChart vars={thisComponent.vars} />
            <BarChart2 vars={thisComponent.vars} />
        );
    }
}

function mapStateToProps(state){
    return {
        components: state.components
    }
}

export default connect(mapStateToProps)(BarChartOnly);
