import React, { Component } from 'react';
import { connect } from 'react-redux';
import MySlider from '../inputs/Slider';

class SliderOnly extends Component {

  render() {
      var thisComponent = this.props.components[this.props.id]
        return (
            <div className="slider">
                {thisComponent.vars.map(function(v){
                    return <MySlider key={v} id={v} />
                })}
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        components: state.components
    }
}

export default connect(mapStateToProps)(SliderOnly);
