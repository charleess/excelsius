import React, { Component } from 'react';
import { connect } from 'react-redux';
import MySlider from '../inputs/Slider';
// import MyBarChart from '../charts/Bar-Chart';
// import MyPieChart from '../charts/Pie-Chart';

import BarChart2 from '../charts/Bar-Chart-V2';
import PieChart2 from '../charts/Pie-Chart-V2';

class GraphAndSliders extends Component {

  render() {
    var thisComponent = this.props.components[this.props.id]

    var graph
    switch(thisComponent.type){
        case 'pie-chart-sliders':
            // graph = <MyPieChart vars={thisComponent.vars}/>
            graph = <PieChart2 vars={thisComponent.vars}/>
            break
        case 'bar-chart-sliders':
            // graph = <MyBarChart vars={thisComponent.vars}/>
            graph = <BarChart2 vars={thisComponent.vars}/>
            break
        // no default
    }

    return (
      <div>
        {thisComponent.vars.map((v, i) => (<MySlider key={i} id={v}/>))}
         {graph} 
      </div>
    );
  }
}

function mapStateToProps(state) {
    return {
        components: state.components
    }
}

export default connect(mapStateToProps)(GraphAndSliders);
