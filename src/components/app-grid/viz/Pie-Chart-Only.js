import React, { Component } from 'react';
import { connect } from 'react-redux';
// import MyPieChart from '../charts/Pie-Chart';

import PieChart2 from '../charts/Bar-Chart-V2';

class PieChartOnly extends Component {

  render() {
      var thisComponent = this.props.components[this.props.id]
        return (
            // <MyPieChart vars={thisComponent.vars} />
            <PieChart2 vars={thisComponent.vars} />
        );
    }
}

function mapStateToProps(state){
    return {
        components: state.components
    }
}

export default connect(mapStateToProps)(PieChartOnly);
