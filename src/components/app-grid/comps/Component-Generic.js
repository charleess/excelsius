import React, { Component } from 'react';
import { connect } from 'react-redux';

import GraphAndSliders from '../viz/Graph-And-Sliders';
import PieChartOnly from '../viz/Pie-Chart-Only';
import BarChartOnly from '../viz/Bar-Chart-Only';
import SliderOnly from '../viz/Slider-Only';

class ComponentGeneric extends Component {
    constructor(props){
        super(props);
        this.state = {
            id: props.id
        }
    }

  render() {
    //For easier syntax
    var thisComponent = this.props.components[this.props.id]
    //Component to render, based on the id
    switch(this.props.components[thisComponent.id].type){
        case "bar-chart-sliders":
            return <GraphAndSliders id={this.props.id} />

        case "pie-chart-sliders":
            return <GraphAndSliders id={this.props.id} />

        case "bar-chart-only":
            return <BarChartOnly id={this.props.id} />

        case "pie-chart-only":
            return <PieChartOnly id={this.props.id} />

        case "slider-only":
            return <SliderOnly id={this.props.id} />

        default:
            return <p className="content">The type of component was not recognized, check the configuration</p>
    }
  }
}

function mapStateToProps(state) {
    return {
        components: state.components
    }
}

export default connect(mapStateToProps)(ComponentGeneric);
