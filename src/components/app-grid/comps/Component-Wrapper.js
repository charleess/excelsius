import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import ComponentGeneric from './Component-Generic';

import { updateComponentData } from '../../../actions/actions-data';

class ComponentWrapper extends Component {
  constructor(props){
    super(props);
    this.state = {
      modal: '',
      modalValid: true,
      showTitle: true,
      id: props.id
    }
  }

  // componentWillMount() {
  //   // Check if the component is viable
  //   const thisComponent = this.props.components[this.props.id]
  //   const variables = this.props.variables
  //   thisComponent.vars.map(function(v){
  //     if(variables[v] === undefined){
  //       thisComponent.vars.splice(thisComponent.vars.indexOf(v),1)
  //     }
  //     return undefined
  //   })
  //   console.log(thisComponent.vars)
  // }

  showModal() {
    this.setState({'modal': ` is-active slideInUp`});
  }

  closeModal() {
    this.setState({'modal': ` is-active slideOutDown`});
  }

  submitModal() {
    this.setState({'modal': ` is-active slideOutDown`});
  }

  handleToggleTitle() {
    this.setState({showTitle: !this.state.showTitle});
  }

  componentValid(thisComponent) {
    // No variables
    if(thisComponent.numberOfVars === 0){
      return false
    }
    // Variables not set
    var err = false
    thisComponent.vars.map(function(v) {
      if((v === "default") || (v === "")){
        err = true
      }
      return undefined
    })
    if(err === true){
      return false
    }

    return true
  }

  render() {
    //For easier syntax
    const thisComponent = this.props.components[this.props.id]
    var componentValid = this.componentValid(thisComponent)

    return (
      <div className="tile is-child box">
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
          <p className='title'>{this.state.showTitle ? thisComponent.name : ""}</p>
          <a>
            <span className="icon is-pulled-right" onClick={this.showModal.bind(this)}>
              <i className="fa fa-cog"></i>
            </span>
          </a>
        </div>

        {componentValid === false ? 
            <p className="has-text-centered">No variables seem to have been selected for this component, go to <Link to="/components">components</Link> to select them</p>
        :
          <figure className="is-4by3">
            <ComponentGeneric id={this.props.id}/>
          </figure>
        }

        <div className={'modal animated' + this.state.modal}>
          <div className="modal-background" onClick={this.closeModal.bind(this)}></div>
            <div className="modal-card">
              <header className="modal-card-head">
                <p className="modal-card-title">Parameters</p>
                <button className="delete" onClick={this.closeModal.bind(this)}></button>
              </header>
              <section className="modal-card-body">
                <div className="tile is-ancestor">
                  <div className="tile is-parent">
                    <div className="tile is-child box">
                      <div className="field">
                        <p className="control">
                          <label className="checkbox">
                              <input type="checkbox" checked={this.state.showTitle} onChange={this.handleToggleTitle.bind(this)}></input>
                              &nbsp;Show title
                          </label>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <footer className="modal-card-foot">
                <a className="button is-success" onClick={this.submitModal.bind(this)}>Save changes</a>
                <a className="button" onClick={this.closeModal.bind(this)}>Cancel</a>
              </footer>
            </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return {
        variables: state.variables,
        components: state.components
    }
}

export default connect(mapStateToProps)(ComponentWrapper);
