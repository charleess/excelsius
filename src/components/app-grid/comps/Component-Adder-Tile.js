import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import ModalParameters from '../../../misc/modal-parameters';

import { createComponent } from '../../../actions/actions-data';
import { addToGrid, removeFromGrid } from '../../../actions/actions-view';

class ComponentAdderTile extends Component {
    constructor(props){
        super(props);
        this.state = {
            modal: '',
            componentToLoad: "default",
            componentToLoadError: '',
        }
    }

    showModal() {
        this.setState({'modal': ` is-active slideInUp`})
    }

    closeModal() {
        this.setState({'modal': ` is-active slideOutDown`})
    }

    submitModal() {
        this.setState({'modal': ` is-active slideOutDown`})
    }

    handleExistingComponentOptionChange(e) {
        this.setState({componentToLoad: e.target.value, componentToLoadError: ''})
    }

    handleLoadExistingComponent() {
        var isValid
        if(this.state.componentToLoad !== "default"){
            this.setState({componentToLoadError: ""});
            isValid = true;
        } else {
            this.setState({componentToLoadError: ` is-danger`});
            isValid = false;
        }
        
        if(isValid){
            //Load the component
            this.props.addToGrid(this.state.componentToLoad);
        }
    }

    render() {
        return (
            <div className="Genric-component">
                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                    <p className='title'>Add Components</p>
                    <a>
                        <span className="icon is-pulled-right" onClick={this.showModal.bind(this)}>
                            <i className="fa fa-cog"></i>
                        </span>
                    </a>
                </div>

                <div className="existing">
                    <label className="label">Choose an existing component</label>
                    <p className="control" style={{display: 'flex', justifyContent: 'space-between'}}>
                        <span className={"select" + this.state.componentToLoadError}>
                            <select onChange={this.handleExistingComponentOptionChange.bind(this)}>
                                <option value="default">Select a component</option>
                                {Object.keys(this.props.components).sort().map(function(componentId){
                                    return(
                                        <option key={componentId} value={componentId}>{this.props.components[componentId].name}</option>
                                    )
                                }.bind(this))}
                            </select>
                        </span>
                    <a className="button" onClick={this.handleLoadExistingComponent.bind(this)}>Load</a>
                    </p>
                </div>
                <hr/>
                <div className="new">
                    <label className="label">Or <Link to="/components">create a new one</Link></label>
                </div>


                <ModalParameters
                    animation={this.state.modal}
                    hide={this.closeModal.bind(this)}
                    submit={this.submitModal.bind(this)} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        components: state.components,
        componentsTypes: state.componentsTypes
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createComponent: createComponent,
        addToGrid: addToGrid,
        removeFromGrid: removeFromGrid
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ComponentAdderTile);
