/* Actions related to the data */
/*    Xcelsius by Kynoa.io     */

export const createVariable = (name, calculated) => {
    return {
        type: 'CREATE_VARIABLE',
        payload: {
            name: name,
            calculated: calculated
        }
    }
}

export const deleteVariable = (id) => {
    return {
        type: 'DELETE_VARIABLE',
        payload: {
            id: id
        }
    }
}

export const updateVariableData = (id, kind, value) => {
    return {
        type: 'UPDATE_VARIABLE_DATA',
        payload: {
            id: id,
            kind: kind,
            value: value
        }
    }
};

export const createComponent = (name, type, variables) => {
    return {
        type: 'CREATE_COMPONENT',
        payload: {
            name: name,
            type: type,
            variables: variables
        }
    }
}

export const deleteComponent = (id) => {
    return {
        type: 'DELETE_COMPONENT',
        payload: {
            id: id
        }
    }
}

export const updateComponentData = (id, kind, value) => {
    return {
        type: 'UPDATE_COMPONENT_DATA',
        payload: {
            id: id,
            kind: kind,
            value: value
        }
    }
};