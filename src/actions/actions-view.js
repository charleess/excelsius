export const addToGrid = (id) => {
    return {
        type: 'ADD_COMPONENT_TO_GRID',
        payload: {
            id: id
        }
    }
};

export const removeFromGrid = (id) => {
    return {
        type: 'REMOVE_COMPONENT_FROM_GRID',
        payload: {
            id: id
        }
    }
};