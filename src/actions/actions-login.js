import sessionApi from '../api/sessionApi';

export const loginSuccess = () => {  
  return {
      type: 'LOG_IN_SUCCESS'
    }
}

export const loginError = () => {  
  return {
      type: 'LOG_IN_ERROR'
    }
}

export const loginUser = (credentials) => {
    return function(dispatch){
        return sessionApi.login(credentials).then(res => {
            sessionStorage.setItem('jwt', res.jwt);
            dispatch(loginSuccess());
        }).catch(error => {
            throw(error)
        })
    }
};