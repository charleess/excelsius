import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
        <footer className="footer" style={{position: 'fixed', minWidth: '100%',bottom: 0, maxHeight: '50px', padding: '1em'}}>
            <div className="container is-fluid">
                <div className="content has-text-centered">
                <p>
                    <strong>Xcelsius</strong> by Kynoa.io.
                </p>
                </div>
            </div>
        </footer>
    );
  }
}

export default Footer;