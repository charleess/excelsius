import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navbar extends Component {
  render() {
    return (
        <nav className="navbar">
            <div className="navbar-brand">
                <Link className="navbar-item" to='/'>
                    Da New Xcelsius
                </Link>

                <span className="navbar-burger burger">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </div>


            <div className="navbar-menu">
                <div className="navbar-end">
                    <Link className='navbar-item' to='/variables'>
                        Variables
                    </Link>
                    <Link className='navbar-item' to='/components'>
                        Components
                    </Link>
                    <Link className='navbar-item' to='/login'>
                        Login
                    </Link>
                </div>
            </div>
        </nav>
    );
  }
}

export default Navbar;