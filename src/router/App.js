import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Navbar from './Navbar';
// import Footer from './Footer';

import AppGrid from '../components/app-grid/App-Grid';
import VariablesGrid from '../components/variables-grid/Variables-Grid';
import ComponentsGrid from '../components/components-grid/Components-Grid';
import LoginPage from '../components/login-page/Login-Page';

import 'bulma/css/bulma.css';
import 'font-awesome/css/font-awesome.css';

const App = () => (
    <div>
        <Navbar />
        <Switch>
            <Route exact path="/" component={AppGrid} />
            <Route path='/variables' component={VariablesGrid} />
            <Route path='/components' component={ComponentsGrid} />

            <Route path='/login' component={LoginPage} />
        </Switch>
        {/* <Footer /> */}
    </div>
)

export default App