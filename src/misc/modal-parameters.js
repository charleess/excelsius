import React from 'react';

const ModalParameters = ({animation, hide, submit}) => {
    return (
        <div className={'modal animated' + animation}>
            <div className="modal-background" onClick={hide}></div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">Parameters</p>
                    <button className="delete" onClick={hide}></button>
                </header>
                <section className="modal-card-body">
                    <div className="tile is-ancestor">
                        <div className="tile is-parent">
                            <div className="tile is-child box">
                                There are no parameters available for this element 
                            </div>
                        </div>
                    </div>
                </section>
                <footer className="modal-card-foot">
                    <a className="button is-primary" onClick={submit}>Save changes</a>
                    <a className="button" onClick={hide}>Cancel</a>
                </footer>
            </div>
        </div>
    )
}

export default ModalParameters