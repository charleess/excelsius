import React from 'react';

const TextInput = ({label, type="text", name, value, placeholder, onChange, error}) => {
    if(!(error === undefined)){
        var danger = ` is-danger`
    } else {
        danger = ''
    }
    return (
        <div className="field">
            <label className="label">{label}</label>
            <p className="control">
                <input
                    className={"input" + danger}
                    type={type}
                    name={name}
                    value={value}
                    placeholder={placeholder}
                    onChange={onChange}/>
            </p>
            {error && <p className="help is-danger">{error}</p>}
        </div>
    )
}

export default TextInput