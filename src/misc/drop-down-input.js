import React from 'react';

const DropDownInput = ({label, name, value, defaultOption, options, optionsLabels, onChange, error}) => {
    if(!(error === undefined)){
        var danger = ` is-danger`
    } else {
        danger = ''
    }
    return (
        <div className="field">
            <label className="label">{label}</label>
            <p className="control">
                <span className={"select" + danger}>
                    <select onChange={onChange} name={name} value={value}>
                        {defaultOption && <option value='default'>{defaultOption}</option>}
                        {options.map((opt) => (<option key={opt} value={opt}>{optionsLabels[opt].name}</option>))}
                    </select>
                </span>
            </p>
            {error && <p className="help is-danger">{error}</p>}
        </div>
    )
}

export default DropDownInput