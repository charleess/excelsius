export default {
    'bar-chart-only': ` fa-bar-chart`,
    'bar-chart-sliders': ` fa-bar-chart`,
    'pie-chart-only': ` fa-pie-chart`,
    'pie-chart-sliders': ` fa-pie-chart`,
    'slider-only': '',
}