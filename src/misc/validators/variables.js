import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput (data, variables, id) {
    let errors = {};
    let currentNames = Object.keys(variables).map((key) => variables[key].name)

    // NAME
    // Name must be something
    if (Validator.isEmpty(data.name)) {
        errors.name = 'This field is required';
    }

    // Name must not exist
    if (currentNames.indexOf(data.name) > -1) {
        errors.name = 'This name is already in use';
    }

    // Except if it is the current component's
    if(id) {
        if (data.name === variables[id].name) {
            delete errors.name
        }
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}