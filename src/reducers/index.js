import {combineReducers} from 'redux';

import componentsReducer from './reducer-components';
import componentsTypesReducer from './reducer-components-types';
import variablesReducer from './reducer-variables';
import gridReducer from './reducer-app-grid';
import sessionReducer from './reducer-session';

export const allReducers = combineReducers({
    components: componentsReducer,
    componentsTypes: componentsTypesReducer,
    variables: variablesReducer,
    appGrid: gridReducer,
    session: sessionReducer
});
