import shallowCopy from '../misc/copier';
import initialState from './initial-state';

const grid = (state = initialState.appGrid, action) => {
    var newState = shallowCopy(state)
    if(action.payload){
        var id = action.payload.id
    }
    switch (action.type) {
    case 'ADD_COMPONENT_TO_GRID':
        newState[id] = {
            id: id
        }
        return newState
    case 'REMOVE_COMPONENT_FROM_GRID':
        delete newState[id]
        return newState

    case 'DELETE_COMPONENT':
        delete newState[id]
        return newState

    // no default
    }
    return state;
};

export default grid