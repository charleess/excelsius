import shallowCopy from '../misc/copier';
import shortid from 'shortid';
import initialState from './initial-state';

const variables = (state = initialState.variables, action) => {
    var newState = shallowCopy(state)
    if(action.payload){
        var {name, calculated, id, kind, value} = action.payload
    }
    switch (action.type) {
    case 'CREATE_VARIABLE':
        var newId = shortid.generate();
        newState[newId] = {
            id: newId,
            calculated: calculated,
            formula: undefined,
            name: name,
            min: 0,
            max: 100,
            value: 10
        }
        return newState

    case 'UPDATE_VARIABLE_DATA':
        newState[id][kind] = value
        return newState

    case 'DELETE_VARIABLE':
        delete newState[id]
        return newState

    // no default
    }
    return state;
};

export default variables