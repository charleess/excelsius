export default {
    appGrid: {},
    components: {},
    componentsTypes: {
            'pie-chart-only': {
            name: 'Simple Pie Chart'
        },
            'bar-chart-only': {
            name: 'Simple Bar Chart'
        },
            'slider-only': {
            name: 'Simple Slider'
        },
            'pie-chart-sliders': {
            name: 'Pie Chart and Sliders'
        },
            'bar-chart-sliders': {
            name: 'Bar Chart and Sliders'
        }
    },
    variables: {},
    session: {}
}