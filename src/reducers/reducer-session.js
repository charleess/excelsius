import initialState from './initial-state';

const session = (state = initialState.session, action) => {
    switch(action.type) {
        case 'LOG_IN_SUCCESS':
            return !!sessionStorage.jwt;
        case 'LOG_OUT':
            return !!sessionStorage.jwt;

        default:
            return state
    }
};

export default session