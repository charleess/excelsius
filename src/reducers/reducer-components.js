import shallowCopy from '../misc/copier';
import shortid from 'shortid';
import initialState from './initial-state';

const data = (state = initialState.components, action) => {
    var newState = shallowCopy(state)
    if(action.payload){
        var {name, type, id, kind, value} = action.payload
    }
    switch (action.type) {
    case 'CREATE_COMPONENT':
        var newId = shortid.generate();
        newState[newId] = {
            id: newId,
            name: name, 
            type: type,
            vars: [],
            numberOfVars: 0
        }
        return newState
    case 'UPDATE_COMPONENT_DATA':
        newState[id][kind] = value
        return newState

    case 'DELETE_COMPONENT':
        delete newState[id]
        return newState

    // no default
    }
    return state;
};

export default data